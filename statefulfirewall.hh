#ifndef CLICK_STATEFULFIREWALL_HH
#define CLICK_STATEFULFIREWALL_HH
#include <click/element.hh>
#include <clicknet/ip.h>
#include <clicknet/tcp.h>
#include <clicknet/udp.h>
#include <click/args.hh>
#include <map>
#include <iostream>
#include <vector>
CLICK_DECLS

using namespace std;

/* Handshake status */
#define SYN 0
#define SYNACK 1
#define HS_DONE 2

class Connection{
private:
    String sourceip;
    String destip;
    int sourceport;
    int destport;
    int proto;
    unsigned long sourceseq;
    unsigned long destseq;
    int handshake_stat;
    bool isfw; //true if forward connection. false if reverse connection.
public:
	Connection(String s, String d, unsigned sp, unsigned dp, unsigned long seq_s, unsigned long seq_d, int pr, bool fwdflag);
	Connection();
	~Connection(){};

	void print() const;
    bool operator==(const Connection &other) const;

    /* Unused function because map was changed to a vector due to map's find() operation working incorrectly */
    // int compare(const Connection other) const;

    String get_sourceip() const;
    String get_destip() const;
    int get_sourceport() const;
    int get_destport() const;
    unsigned long get_sourceseq();
    unsigned long get_destseq();
    void set_sourceseq(unsigned long seq_s);
    void set_destseq(unsigned long seq_d);
    int get_proto() const;
    int get_handshake_stat();
    void update_handshake_stat();
    bool is_forward() const;
};

class Policy{
private:
	String sourceip;
	String destip;
	int sourceport;
	int destport;
	int proto;
	int action;
public:
	Policy(String s, String d, int sp, int dp, int p, int act);
	~Policy(){};

	Connection getConnection();
	int getAction();
};

/* Unused structure because map was changed to a vector due to map's find() operation working incorrectly */
// struct cmp_connection
// {
//    bool operator()(Connection const a, Connection const b)
//    {
//       return a.compare(b) < 0;
//    }
// };

class StatefulFirewall : public Element {
private:
    /* Map was changed to a vector due to map's find() operation working incorrectly */
	// std::map<Connection,int,cmp_connection> Connections; //Map of connections to their actions.
    std::vector<Connection> Connections;
    std::vector<int> Actions;
	std::vector<Policy> list_of_policies;
public:
	StatefulFirewall(){};
    ~StatefulFirewall(){};

    int configure(Vector<String> &conf, ErrorHandler *errh);

    const char *class_name() const		{ return "StatefulFirewall"; }
    const char *port_count() const		{ return "1/2"; }
    const char *processing() const		{ return PUSH; }
    // this element does not need AlignmentInfo; override Classifier's "A" flag
    const char *flags() const			{ return ""; }

    bool check_if_new_connection(const Packet *p);
    bool check_if_connection_reset(const Packet *p);
    void add_connection(Connection &c, int action);
    void delete_connection(Connection &c);
    int find_connection(Connection &c);
    Connection get_canonicalized_connection(const Packet *p);
    void read_policy_config(String s);
    void dotted_addr(const uint32_t addr, String &s);
    int filter_packet(const Packet *p);
    void push(int port, Packet *p);

    /* The default action configured for the firewall. */
    int DEFAULTACTION = 0;
};

CLICK_ENDDECLS
#endif
