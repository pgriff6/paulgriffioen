/*
 * statefulipfilter.{cc,hh} -- Stateful IP-packet filter
 *
 */

#include <click/config.h>
#include <click/confparse.hh>
#include "statefulfirewall.hh"
CLICK_DECLS

Connection::Connection(String s, String d, unsigned sp, unsigned dp, unsigned long seq_s, unsigned long seq_d, int pr, bool fwdflag)
{
    sourceip = s;
    destip = d;
    sourceport = sp;
    destport = dp;
    sourceseq = seq_s;
    destseq = seq_d;
    proto = pr;
    isfw = fwdflag;
    handshake_stat = SYN;
}
Connection::Connection()
{
    sourceip = "";
    destip = "";
    sourceport = 0;
    destport = 0;
    sourceseq = 0;
    destseq = 0;
    proto = 0;
    isfw = true;
    handshake_stat = SYN;
}

/* Useful for debugging */
void Connection::print() const
{
    click_chatter("Connection");
    click_chatter("Source IP: %s", sourceip.c_str());
    click_chatter("Destination IP: %s", destip.c_str());
    click_chatter("Source Port: %u", sourceport);
    click_chatter("Destination Port: %u", destport);
    click_chatter("Source Sequence #: %lu", sourceseq);
    click_chatter("Destination Sequence #: %lu", destseq);
    click_chatter("Protocol: %d", proto);
    click_chatter("Is Forward: %d", isfw);
    click_chatter("Handshake (SYN=0,SYNACK=1,HS_DONE=2): %d", handshake_stat);
}

/* Overlaod == operator to check if two Connection objects are equal.
 * Ignore the isfw flag if protocol is TCP. Return true if equal. false otherwise. */
bool Connection::operator==(const Connection &other) const
{
	if((sourceip.compare(other.get_sourceip()) == 0) && (destip.compare(other.get_destip()) == 0) && (proto == other.get_proto()))
	{
	    if((sourceport == other.get_sourceport()) && (destport == other.get_destport()))
	    {
            if(proto == 6) // TCP
            {
                return true;
            }
            else if(isfw == other.is_forward())
            {
                return true;
            }
	    }
	}
	return false;
}

/* Unused function because map was changed to a vector due to map's find() operation working incorrectly */
// /*Compare two connections to determine the sequence in map.*/
// int Connection::compare(const Connection other) const
// {
//     //Return negative number if current instance is less than argument; return positive number otherwise
//     int srcip1, srcip2, srcip3, srcip4, srcip1o, srcip2o, srcip3o, srcip4o, dstip1, dstip2, dstip3, dstip4, dstip1o, dstip2o, dstip3o, dstip4o;
//     const char *srcip;
//     srcip = sourceip.c_str();
//     int j = 0;
//     int k = 0;
//     char Srcip1[5];
//     char Srcip2[5];
//     char Srcip3[5];
//     char Srcip4[5];
//     while(srcip[j] != '.')
//     {
//         Srcip1[k] = srcip[j];
//         j++; k++;
//     }
//     srcip1 = atoi(Srcip1);
//     k = 0; j++;
//     while(srcip[j] != '.')
//     {
//         Srcip2[k] = srcip[j];
//         j++; k++;
//     }
//     srcip2 = atoi(Srcip2);
//     k = 0; j++;
//     while(srcip[j] != '.')
//     {
//         Srcip3[k] = srcip[j];
//         j++; k++;
//     }
//     srcip3 = atoi(Srcip3);
//     k = 0; j++;
//     while(srcip[j] != '\0')
//     {
//         Srcip4[k] = srcip[j];
//         j++; k++;
//     }
//     srcip4 = atoi(Srcip4);
//     const char *srcipo;
//     srcipo = other.get_sourceip().c_str();
//     char Srcip1o[5];
//     char Srcip2o[5];
//     char Srcip3o[5];
//     char Srcip4o[5];
//     j = 0; k = 0;
//     while(srcipo[j] != '.')
//     {
//         Srcip1o[k] = srcipo[j];
//         j++; k++;
//     }
//     srcip1o = atoi(Srcip1o);
//     k = 0; j++;
//     while(srcipo[j] != '.')
//     {
//         Srcip2o[k] = srcipo[j];
//         j++; k++;
//     }
//     srcip2o = atoi(Srcip2o);
//     k = 0; j++;
//     while(srcipo[j] != '.')
//     {
//         Srcip3o[k] = srcipo[j];
//         j++; k++;
//     }
//     srcip3o = atoi(Srcip3o);
//     k = 0; j++;
//     while(srcipo[j] != '\0')
//     {
//         Srcip4o[k] = srcipo[j];
//         j++; k++;
//     }
//     srcip4o = atoi(Srcip4o);
//     const char *dstip;
//     dstip = destip.c_str();
//     char Dstip1[5];
//     char Dstip2[5];
//     char Dstip3[5];
//     char Dstip4[5];
//     j = 0; k = 0;
//     while(dstip[j] != '.')
//     {
//         Dstip1[k] = dstip[j];
//         j++; k++;
//     }
//     dstip1 = atoi(Dstip1);
//     k = 0; j++;
//     while(dstip[j] != '.')
//     {
//         Dstip2[k] = dstip[j];
//         j++; k++;
//     }
//     dstip2 = atoi(Dstip2);
//     k = 0; j++;
//     while(dstip[j] != '.')
//     {
//         Dstip3[k] = dstip[j];
//         j++; k++;
//     }
//     dstip3 = atoi(Dstip3);
//     k = 0; j++;
//     while(dstip[j] != '\0')
//     {
//         Dstip4[k] = dstip[j];
//         j++; k++;
//     }
//     dstip4 = atoi(Dstip4);
//     const char *dstipo;
//     dstipo = other.get_destip().c_str();
//     char Dstip1o[5];
//     char Dstip2o[5];
//     char Dstip3o[5];
//     char Dstip4o[5];
//     j = 0; k = 0;
//     while(dstipo[j] != '.')
//     {
//         Dstip1o[k] = dstipo[j];
//         j++; k++;
//     }
//     dstip1o = atoi(Dstip1o);
//     k = 0; j++;
//     while(dstipo[j] != '.')
//     {
//         Dstip2o[k] = dstipo[j];
//         j++; k++;
//     }
//     dstip2o = atoi(Dstip2o);
//     k = 0; j++;
//     while(dstipo[j] != '.')
//     {
//         Dstip3o[k] = dstipo[j];
//         j++; k++;
//     }
//     dstip3o = atoi(Dstip3o);
//     k = 0; j++;
//     while(dstipo[j] != '\0')
//     {
//         Dstip4o[k] = dstipo[j];
//         j++; k++;
//     }
//     dstip4o = atoi(Dstip4o);
//     if(srcip1 < srcip1o)
//     {
//         return -1;
//     }
//     else if(srcip1 == srcip1o)
//     {
//         if(srcip2 < srcip2o)
//         {
//             return -1;
//         }
//         else if(srcip2 == srcip2o)
//         {
//             if(srcip3 < srcip3o)
//             {
//                 return -1;
//             }
//             else if(srcip3 == srcip3o)
//             {
//                 if(srcip4 < srcip4o)
//                 {
//                     return -1;
//                 }
//                 else if(srcip4 == srcip4o)
//                 {
//                     if(dstip1 < dstip1o)
//                     {
//                         return -1;
//                     }
//                     else if(dstip1 == dstip1o)
//                     {
//                         if(dstip2 < dstip2o)
//                         {
//                             return -1;
//                         }
//                         else if(dstip2 == dstip2o)
//                         {
//                             if(dstip3 < dstip3o)
//                             {
//                                 return -1;
//                             }
//                             else if(dstip3 == dstip3o)
//                             {
//                                 if(dstip4 < dstip4o)
//                                 {
//                                     return -1;
//                                 }
//                             }
//                         }
//                     }
//                 }
//             }
//         }
//     }
//     return 1;
// }

String Connection::get_sourceip() const
{
    return sourceip;
}

String Connection::get_destip() const
{
    return destip;
}

int Connection::get_sourceport() const
{
    return sourceport;
}

int Connection::get_destport() const
{
    return destport;
}

unsigned long Connection::get_sourceseq()
{
    return sourceseq;
}

unsigned long Connection::get_destseq()
{
    return destseq;
}

void Connection::set_sourceseq(unsigned long seq_s)
{
    sourceseq = seq_s;
}

void Connection::set_destseq(unsigned long seq_d)
{
    destseq = seq_d;
}

int Connection::get_proto() const
{
    return proto;
}

int Connection::get_handshake_stat()
{
    return handshake_stat;
}

/* Update the status of the handshake */
void Connection::update_handshake_stat()
{
    if(handshake_stat == SYN)
    {
        handshake_stat = SYNACK;
    }
    else if(handshake_stat == SYNACK)
    {
        handshake_stat = HS_DONE;
    }
}

/* Return value of isfw */
bool Connection::is_forward() const
{
    return isfw;
}

Policy::Policy(String s, String d, int sp, int dp, int p, int act)
{
    sourceip = s;
    destip = d;
    sourceport = sp;
    destport = dp;
    proto = p;
    action = act;
}

/* Return a Connection object representing policy */
Connection Policy::getConnection()
{
    return Connection(sourceip, destip, sourceport, destport, 0, 0, proto, true);
}

/* Return action for this Policy */
int Policy::getAction()
{
    return action;
}

/* Take the configuration paramenters as input corresponding to
 * POLICYFILE and DEFAULT where
 * POLICYFILE : Path of policy file
 * DEFAULT : Default action (0/1) */
int StatefulFirewall::configure(Vector<String> &conf, ErrorHandler *errh)
{
    String policy_file;

    if(Args(conf, this, errh)
    .read("POLICYFILE", policy_file)
    .read("DEFAULT", DEFAULTACTION)
    .complete() < 0)
    return -1;

    read_policy_config(policy_file);
    return 0;
}

/* return true if Packet represents a new connection
 * i.e., check if the connection exists in the vector.
 * Check the SYN flag in the header to be sure for TCP protocol.
 * else return false.
 */
bool StatefulFirewall::check_if_new_connection(const Packet *p)
{
    Connection c = get_canonicalized_connection(p);
    if(find_connection(c) == -1)
    {
        if(c.get_proto() == 6) // TCP
        {
            const click_tcp *tcphead;
            tcphead = p->tcp_header();
            uint8_t flag = tcphead->th_flags; // FIN=0x01 SYN=0x02 RST=0x04 ACK=0x10
            if(flag != 0x02)
            {
                return false;
            }
        }
        return true;
    }
    return false;
}

/* Check if the packet represents Connection reset
 * i.e., if the RST flag is set in the header for TCP protocol.
 * Return true if connection reset
 * else return false. */
bool StatefulFirewall::check_if_connection_reset(const Packet *p)
{
    const click_ip *iphead;
    iphead = p->ip_header();
    uint8_t proto = iphead->ip_p;
    if(proto == 6) // TCP
    {
        const click_tcp *tcphead;
        tcphead = p->tcp_header();
        uint8_t flag = tcphead->th_flags; // FIN=0x01 SYN=0x02 RST=0x04 ACK=0x10
        if(flag == 0x04)
        {
            return true;
        }
    }
    return false;
}

/* Add a new connection to the vector and add its action to the associated vector.*/
void StatefulFirewall::add_connection(Connection &c, int action)
{
    Connections.push_back(c);
    Actions.push_back(action);
}

/* Delete the connection from both the connection and action vectors */
void StatefulFirewall::delete_connection(Connection &c)
{
    int it = find_connection(c);
    Connections.erase(Connections.begin()+it);
    Actions.erase(Actions.begin()+it);
}

/* Return the index of the connection in the vector */
int StatefulFirewall::find_connection(Connection &c)
{
    for(int it = 0; it < Connections.size(); it++)
    {
        if(c == Connections.at(it))
        {
            return it;
        }
    }
    return -1;
}

/* Create a new connection object for Packet.
 * Canonicalize the source and destination ip address and port number.
 * i.e, make the source less than the destination and
 * update isfw to false if swapping source and destination. */
Connection StatefulFirewall::get_canonicalized_connection(const Packet *p)
{
    const click_ip *iphead;
    iphead = p->ip_header();
    uint32_t srcaddr = iphead->ip_src.s_addr;
    uint32_t dstaddr = iphead->ip_dst.s_addr;
    uint8_t proto = iphead->ip_p;
    String saddr;
    String daddr;
    dotted_addr(srcaddr, saddr);
    dotted_addr(dstaddr, daddr);

    uint16_t srcport = 0;
    uint16_t dstport = 0;
    uint32_t seqnum = 0;
    uint32_t acknum = 0;

    if(proto == 6) // TCP
    {
        const click_tcp *tcphead;
        tcphead = p->tcp_header();
        srcport = ntohs(tcphead->th_sport);
        dstport = ntohs(tcphead->th_dport);
        seqnum = ntohl(tcphead->th_seq);
        acknum = ntohl(tcphead->th_ack);
    }
    else if(proto == 17) // UDP
    {
        const click_udp *udphead;
        udphead = p->udp_header();
        srcport = ntohs(udphead->uh_sport);
        dstport = ntohs(udphead->uh_dport);
    }

    if(ntohl(srcaddr) <= ntohl(dstaddr))
    {
        return Connection(saddr, daddr, srcport, dstport, seqnum, acknum, proto, true); // Source address is lower than destination address
    }
    else
    {
        return Connection(daddr, saddr, dstport, srcport, acknum, seqnum, proto, false); // Destination address is lower than source address
    }
}

/* Read policy from a config file whose path is passed as parameter.
 * Update the policy database.
 * Policy config file structure is space separated list of
 * <source_ip source_port destination_ip destination_port protocol action>
 * Add Policy objects to the list_of_policies
 * */
void StatefulFirewall::read_policy_config(String s)
{
    FILE *myfile;
    myfile = fopen(s.c_str(),"r");
    if(myfile != NULL)
    {
        int c, i, k;
        char srcip[20];
        char srcport[10];
        char dstip[20];
        char dstport[10];
        char proto[5];
        char action[5];
        while((c = getc(myfile)) != '\n')
        { // Ignore first line of policy config file
        }
        while(((c = getc(myfile)) != '\n') && (c != EOF))
        {
            for(k = 0; k < 20; k++)
            {
                srcip[k] = '\0'; // Clear character array
                dstip[k] = '\0'; // Clear character array
            }
            for(k = 0; k < 10; k++)
            {
                srcport[k] = '\0'; // Clear character array
                dstport[k] = '\0'; // Clear character array
            }
            for(k = 0; k < 5; k++)
            {
                proto[k] = '\0'; // Clear character array
                action[k] = '\0'; // Clear character array
            }
            i = 0;
            while(c != ' ')
            {
                srcip[i] = char(c);
                c = getc(myfile);
                i++;
            }
            c = getc(myfile);
            i = 0;
            while(c != ' ')
            {
                srcport[i] = char(c);
                c = getc(myfile);
                i++;
            }
            c = getc(myfile);
            i = 0;
            while(c != ' ')
            {
                dstip[i] = char(c);
                c = getc(myfile);
                i++;
            }
            c = getc(myfile);
            i = 0;
            while(c != ' ')
            {
                dstport[i] = char(c);
                c = getc(myfile);
                i++;
            }
            c = getc(myfile);
            i = 0;
            while(c != ' ')
            {
                proto[i] = char(c);
                c = getc(myfile);
                i++;
            }
            c = getc(myfile);
            i = 0;
            while(c != '\n')
            {
                action[i] = char(c);
                c = getc(myfile);
                i++;
            }
            // Add policy to vector
            list_of_policies.push_back(Policy(String(srcip), String(dstip), atoi(srcport), atoi(dstport), atoi(proto), atoi(action)));
        }
        fclose(myfile);
    }
}

/* Convert the integer ip address to string in dotted format.
 * Store the string in s. */
void StatefulFirewall::dotted_addr(const uint32_t addr, String &s)
{
    char hexaddr[10];
    char ip1[3];
    char ip2[3];
    char ip3[3];
    char ip4[3];
    int k;
    for(k = 0; k < 10; k++)
    {
        hexaddr[k] = '\0'; // Clear character array
    }
    for(k = 0; k < 3; k++)
    {
        ip1[k] = '\0'; // Clear character array
        ip2[k] = '\0'; // Clear character array
        ip3[k] = '\0'; // Clear character array
        ip4[k] = '\0'; // Clear character array
    }
    uint32_t newaddr = ntohl(addr); // Convert from network to host byte order
    sprintf(hexaddr, "%x", newaddr); // Convert to character array
    k = 0;
    while(hexaddr[k] != '\0')
    {
        k++;
    }
    if(k == 7)
    {
        ip1[0] = hexaddr[0];
        ip2[0] = hexaddr[1];
        ip2[1] = hexaddr[2];
        ip3[0] = hexaddr[3];
        ip3[1] = hexaddr[4];
        ip4[0] = hexaddr[5];
        ip4[1] = hexaddr[6];
    }
    else
    {
        ip1[0] = hexaddr[0];
        ip1[1] = hexaddr[1];
        ip2[0] = hexaddr[2];
        ip2[1] = hexaddr[3];
        ip3[0] = hexaddr[4];
        ip3[1] = hexaddr[5];
        ip4[0] = hexaddr[6];
        ip4[1] = hexaddr[7];
    }
    long int addr1 = strtol(ip1, NULL, 16);
    long int addr2 = strtol(ip2, NULL, 16);
    long int addr3 = strtol(ip3, NULL, 16);
    long int addr4 = strtol(ip4, NULL, 16);
    char s_tmp[20];
    sprintf(s_tmp, "%d.%d.%d.%d", addr1, addr2, addr3, addr4); // Convert to dotted decimal format
    s = String(s_tmp);
}

/* Check if Packet belongs to new connection.
* If new connection, apply the policy on this packet
* and add the result to the connection map.
* Else return the action in map.
* If Packet indicates connection reset,
* delete the connection from connection map.
*
* Return 1 if packet is allowed to pass
* Return 0 if packet is to be discarded
*/
int StatefulFirewall::filter_packet(const Packet *p)
{
    if(check_if_new_connection(p)) // New connection
    {
        Connection c = get_canonicalized_connection(p);
        for(int i = 0; i < list_of_policies.size(); i++) // Find policy matching connection
        {
            Connection cc = list_of_policies[i].getConnection();
            if(c == cc)
            {
                int action = list_of_policies[i].getAction(); // Apply policy to connection
                add_connection(c, action); // Add connection to vector
                return action;
            }
        }
        add_connection(c, DEFAULTACTION); // Add connection with default action if no matching policy is found
        return DEFAULTACTION;
    }
    else // Not a new connection
    {
        Connection c = get_canonicalized_connection(p);
        int it = find_connection(c);
        if(it != -1) // If connection is in vector
        {
            int returnval = Actions.at(it); // Get action associated with connection
            if(check_if_connection_reset(p))
            {
                Connection cc = Connections.at(it); // Get connection stored in vector
                unsigned long srcseq = c.get_sourceseq();
                unsigned long dstseq = c.get_destseq();
                if(((abs(srcseq-cc.get_sourceseq()) < 2500) && (dstseq == cc.get_destseq())) || ((abs(dstseq-cc.get_destseq()) < 2500) && (srcseq == cc.get_sourceseq())))
                {
                }
                else
                {
                    return 0; // Drop packet if sequence numbers do not fall within certain window
                }
                delete_connection(c); // Remove connection from vector if connection is reset
            }
            else if(c.get_proto() == 6) // TCP
            {
                Connection cc = Connections.at(it); // Get connection stored in vector
                unsigned long srcseq = c.get_sourceseq();
                unsigned long dstseq = c.get_destseq();
                const click_tcp *tcphead;
                tcphead = p->tcp_header();
                uint8_t flag = tcphead->th_flags; // FIN=0x01 SYN=0x02 RST=0x04 ACK=0x10
                if(flag == 0x12) // SYNACK
                {
                    if((srcseq != cc.get_sourceseq()+1) && (dstseq != cc.get_destseq()+1))
                    {
                        return 0; // Drop packet if sequence numbers do not align with typical SYNACK sequence numbers
                    }
                }
                else if(((abs(srcseq-cc.get_sourceseq()) < 2500) && (dstseq == cc.get_destseq())) || ((abs(dstseq-cc.get_destseq()) < 2500) && (srcseq == cc.get_sourceseq())))
                {
                }
                else
                {
                    return 0; // Drop packet if sequence numbers do not fall within certain window
                }
                cc.set_sourceseq(srcseq); // Update source sequence number for connection
                cc.set_destseq(dstseq); // Update destination sequence number for connection
                if(((flag == 0x12) && (cc.get_handshake_stat() == SYN)) || ((flag == 0x10) && (cc.get_handshake_stat() == SYNACK)))
                {
                    cc.update_handshake_stat(); // Update handshake statistic if packet contains SYNACK or ACK
                }
                else if(cc.get_handshake_stat() != HS_DONE)
                {
                    return 0; // Drop all packets with incomplete 3-way handshakes
                }
                delete_connection(c);
                add_connection(cc, returnval); // Replace connection in vector with updated connection
            }
            return returnval;
        }
        else // Not a new connection and not stored in vector (new TCP connection without SYN)
        {
            add_connection(c, DEFAULTACTION); // Apply default action for new TCP connections without SYN
            return DEFAULTACTION;
        }
    }
}

/* Push valid traffic on port 1
* Push discarded traffic on port 0 */
void StatefulFirewall::push(int port, Packet *p)
{
    output(filter_packet(p)).push(p);
}

CLICK_ENDDECLS
EXPORT_ELEMENT(StatefulFirewall)
